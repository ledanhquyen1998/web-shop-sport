<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>List Clothes Country</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/listClothesCt.css') ?>">
</head>
<body>
<div class="header">
	<div class="container">
		<div id="logo">
			<a href=""><img src="<?php echo base_url('logo/logo.png') ?>" alt=""></a>
		</div>
		<div id="support">
			<img src="https://img.icons8.com/carbon-copy/30/000000/phone-not-being-used.png"/>
			<div id="hotline">
				<p>Hotline</p>
				<h5>0975.977.498</h5>
			</div>
		</div>
		<div id="search">
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success" type="submit"><img
							src="https://img.icons8.com/ios-filled/23/000000/search.png"/></button>
			</form>
		</div>
		<div id="login">
			<ul style="list-style: none">
				<li class="nav-item dropdown">
					<?php if (!$this->session->userdata('user')) { ?>
						<a href="<?php echo site_url('login') ?>">Đăng Nhập</a>
					<?php } else { ?>
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
						   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $this->session->userdata('user')->name ?>
						</a>
					<?php } ?>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Trang Cá Nhân</a>
						<a class="dropdown-item" href="#"></a>
						<a class="dropdown-item" href="<?php echo site_url('logout') ?>">Đăng Xuất</a>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<div id="menu" style="height: 51px">
		<div class="container">
			<ul>
				<li>
					<a href="<?php echo site_url('home') ?>">Trang chủ</a>
				</li>
				<li>
					<a href="">Áo CLB</a>
				</li>
				<li>
					<a href="<?php echo site_url('list-clothes-country') ?>">Áo tuyển quốc gia</a>
				</li>
				<li>
					<a href="">Áo không logo</a>
				</li>
				<li>
					<a href="">Giày bóng đá</a>
				</li>
				<li>
					<a href="">Phụ kiện</a>
				</li>
				<li>
					<a href="">Áo khoác</a>
				</li>
				<li>
					<a href="">Áo dài tay</a>
				</li>
				<li>
					<a href="">Bóng</a>
				</li>
				<li>
					<!-- Button trigger modal -->
					<button class="upload-button"
							style="border: white; background: darkblue; color: white; height: 40px"
							type="button" data-toggle="modal" data-target="#exampleModal">
						Upload
					</button>

					<!-- Modal -->
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
						 aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 style="text-align: center;margin-left: auto; font-weight: bold; font-size: 20px">
										Loại Sản
										Phẩm Muốn Tải Lên</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<a class="btn btn-warning ml-5" href="<?php echo site_url('create-new-clothes') ?>"
									   style="font-size: 17px; font-weight: bold">Áo Bóng Đá</a>
									<a class="btn btn-info ml-4" href="<?php echo site_url('create-new-shoes') ?>"
									   style="font-size: 17px; font-weight: bold">Giày Bóng Đá</a>
									<a class="btn btn-success ml-4" href="<?php echo site_url('create-new-ball') ?>"
									   style="font-size: 17px; font-weight: bold">Bóng</a>
								</div>
							</div>
						</div>
					</div>

				</li>
			</ul>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php foreach ($clothesCts as $key => $clothesCt): ?>
				<div class="card col-lg-4" style="width: 18rem;">
					<img style="width: 100%; margin: 0px auto" src="<?php echo base_url()?>clothes/<?php echo $clothesCt->image?>" class="card-img-top" alt="...">
					<div class="card-body">
						<h5 class="card-title">Card title</h5>
						<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
						<a href="#" class="btn btn-primary">Go somewhere</a>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
<?php //print_r($clothesCts)?>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
