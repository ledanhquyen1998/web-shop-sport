<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Clothes Detail</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
		  integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/clothesDetail.css') ?>">
</head>
<body>
<div class="header">
	<div class="container">
		<div id="logo">
			<a href="<?php echo site_url('home') ?>"><img src="<?php echo base_url('logo/logo.png') ?>" alt=""></a>
		</div>
		<div id="support">
			<img src="https://img.icons8.com/carbon-copy/30/000000/phone-not-being-used.png"/>
			<div id="hotline">
				<p>Hotline</p>
				<h5>0975.977.498</h5>
			</div>
		</div>
		<div id="search">
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success" type="submit"><img
							src="https://img.icons8.com/ios-filled/23/000000/search.png"/></button>
			</form>
		</div>
		<div id="login">
			<a href="<?php echo site_url('login') ?>">Đăng Nhập</a>
		</div>
	</div>
	<div id="menu">
		<div class="container">
			<ul>
				<li>
					<a href="<?php echo site_url('home') ?>">Trang chủ</a>
				</li>
				<li>
					<a href="">Áo CLB</a>
				</li>
				<li>
					<a href="">Áo tuyển quốc gia</a>
				</li>
				<li>
					<a href="">Áo không logo</a>
				</li>
				<li>
					<a href="">Giày bóng đá</a>
				</li>
				<li>
					<a href="">Phụ kiện</a>
				</li>
				<li>
					<a href="">Áo khoác</a>
				</li>
				<li>
					<a href="">Áo dài tay</a>
				</li>
				<li>
					<a href="">Bóng</a>
				</li>
				<li>
					<a href="<?php echo site_url('create-new-clothes') ?>">upload</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="container">
		<div class="card mt-5">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-6">
						<img src="<?php echo base_url() ?>clothes/<?php echo $clothesById->image ?>"
							 style="width: 500px; margin: 0 auto">
					</div>
					<div class="col-lg-6">
						<h1><?php echo $clothesById->title ?></h1>
						<div>
							<form action="<?php echo site_url('order-clothes') ?>/<?php echo $clothesById->id ?>"
								  method="post">
								<input name="titleClothes" type="text" value="<?php echo $clothesById->title ?>"
									   style="display: none" id="titleClothes">
								<div class="row">
									<div class="rol-lg-2 ml-3 mt-2">
										Giá:
									</div>
									<div class="col-lg-10" style="text-align: left; margin-left: 49px">
										<div>
											<span><input id="price_1" name="price_1" value="30000"
														 type="radio"></span>
											<span style="color: red; font-weight: bold;font-size: 25px">30.000 đ</span>
											<span>(Vải gai xốp)</span>
										</div>
										<div>
											<span><input id="price_2" name="price_1" value="70000"
														 type="radio"></span>
											<span style="color: red; font-weight: bold;font-size: 25px">70.000 đ</span>
											<span>(Vải thun mịn)</span>
										</div>
										<div>
											<span><input id="price_3" name="price_1" value="100000"
														 type="radio"></span>
											<span style="color: red; font-weight: bold;font-size: 25px">100.000 đ</span>
											<span>(Hàng ST)</span>
										</div>
										<div>
											<span><input id="price_4" name="price_1" value="160000"
														 type="radio"></span>
											<span style="color: red; font-weight: bold;font-size: 25px">160.000 đ</span>
											<span>(Hàng ET)</span>
										</div>
										<div>
											<span><input name="price_1" id="price_5" value="200000"
														 type="radio"></span>
											<span style="color: red; font-weight: bold;font-size: 25px">200.000 đ</span>
											<span>(Hàng body fit player)</span>
										</div>
									</div>
								</div>
								<div class="row mt-4">
									<div class="col-lg-3">
										Số lượng:
									</div>
									<div class="col-lg-9">
										<span>
											<input id="input-order" class="form-group" type="number" value="1"
												   style="width: 50px; text-align: center"
												   name="number">
										</span>
										<span>
											<button type="submit"
													class="btn btn-danger ml-3 button-order" id="button-order">ĐẶT MUA HÀNG</button>
										</span>
									</div>
									<div style="text-align: center;font-size: 20px;color: red">
										<?php echo $this->session->flashdata('fail') ?>
									</div>
									<div style="color: springgreen; font-size: 20px; text-align: center">
										<?php echo $this->session->flashdata('success') ?>
									</div>
							</form>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

