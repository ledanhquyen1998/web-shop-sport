<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Ball Detail</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
		  integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/ballDetail.css') ?>">
</head>
<body>
<div class="header">
	<div class="container">
		<div id="logo">
			<a href="<?php echo site_url('home') ?>"><img src="<?php echo base_url('logo/logo.png') ?>" alt=""></a>
		</div>
		<div id="support">
			<img src="https://img.icons8.com/carbon-copy/30/000000/phone-not-being-used.png"/>
			<div id="hotline">
				<p>Hotline</p>
				<h5>0975.977.498</h5>
			</div>
		</div>
		<div id="search">
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control" type="search" placeholder="Search" aria-label="Search">
				<button class="btn btn-outline-success" type="submit"><img
							src="https://img.icons8.com/ios-filled/23/000000/search.png"/></button>
			</form>
		</div>
		<div id="login">
			<a href="<?php echo site_url('login') ?>">Đăng Nhập</a>
		</div>
	</div>
	<div id="menu">
		<div class="container">
			<ul>
				<li>
					<a href="<?php echo site_url('home') ?>">Trang chủ</a>
				</li>
				<li>
					<a href="">Áo CLB</a>
				</li>
				<li>
					<a href="">Áo tuyển quốc gia</a>
				</li>
				<li>
					<a href="">Áo không logo</a>
				</li>
				<li>
					<a href="">Giày bóng đá</a>
				</li>
				<li>
					<a href="">Phụ kiện</a>
				</li>
				<li>
					<a href="">Áo khoác</a>
				</li>
				<li>
					<a href="">Áo dài tay</a>
				</li>
				<li>
					<a href="">Bóng</a>
				</li>
				<li>
					<a href="<?php echo site_url('create-new-clothes') ?>">upload</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="container mt-5">
		<form action="<?php echo site_url('order-ball') ?>/<?php echo $ball->id ?>" method="post">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-5">
							<img src="<?php echo base_url() ?>ball/<?php echo $ball->imageball ?>" style="width: 100%"
								 alt="">
						</div>
						<div class="col-lg-7">
							<h1><?php echo $ball->titleball ?></h1>
							<div>
								<span>Giá:</span>
								<span class="ml-4" style="color: red; font-size: 30px; font-weight: bold">
									<input name="price_1" value="<?php echo $ball->priceball ?>" checked class="mr-2"
										   type="radio"><?php echo $ball->priceball . ' đ' ?>
								</span>
								<span>(Hàng chính hãng)</span>
							</div>
							<div class="mt-4">
								<div class="row">
									<div class="col-lg-2">
										Số Lượng:
									</div>
									<div class="col-lg-10">
										<input name="number" type="number" value="1"
											   style="width: 50px; text-align: center"
											   class="form-group">
										<button type="submit" class="btn btn-danger ml-2">ĐẶT MUA HÀNG</button>
									</div>
								</div>
								<div style="color: springgreen; font-size: 20px">
									<?php echo $this->session->flashdata('success') ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
		integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
		integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
		crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

