<!DOCTYPE html>
<html lang="en">
<head>
	<title>Add New Ball</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url() ?>vendorFormAddBall/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url() ?>fontsFormAddBall/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendorFormAddBall/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css"
		  href="<?php echo base_url() ?>vendorFormAddBall/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>vendorFormAddBall/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>cssFormAddBall/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>cssFormAddBall/main.css">
	<!--===============================================================================================-->
</head>
<body>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100" style="background: #cad1d9">
			<div class="login100-pic js-tilt" data-tilt>
				<img src="<?php echo base_url() ?>logo/ball.jpg" alt="IMG" style="width: 100%">
			</div>

			<form class="login100-form validate-form" method="post"
				  action="<?php echo site_url('create-ball-success') ?>" enctype="multipart/form-data">
					<span class="login100-form-title">
						Thêm Mới Sản Phẩm
					</span>

				<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
					<input class="input100" type="text" name="titleball" placeholder="Tiêu Đề Sản Phẩm">
				</div>

				<div class="wrap-input100 validate-input" data-validate="Password is required">
					<input class="input100" type="text" name="priceball" placeholder="Giá Sản Phẩm">
				</div>
				<div class="wrap-input100 validate-input" data-validate="Password is required">
					<input class="form-group" type="file" name="imageball" placeholder="Giá Sản Phẩm">
				</div>

				<div class="container-login100-form-btn">
					<a class="btn btn-dark login100-form-btn1" href="<?php echo site_url('home') ?>">Hủy</a>
					<button class="login100-form-btn ml-2" type="submit">
						Tải Lên
					</button>
				</div>
			</form>
		</div>
	</div>
</div>


<!--===============================================================================================-->
<script src="<?php echo base_url() ?>vendorFormAddBall/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>vendorFormAddBall/bootstrap/js/popper.js"></script>
<script src="<?php echo base_url() ?>vendorFormAddBall/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>vendorFormAddBall/select2/select2.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>vendorFormAddBall/tilt/tilt.jquery.min.js"></script>
<script>
	$('.js-tilt').tilt({
		scale: 1.1
	})
</script>
<!--===============================================================================================-->
<script src="<?php echo base_url() ?>js/main.js"></script>

</body>
</html>
