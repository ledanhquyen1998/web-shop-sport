<?php


class BallModel extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function getAllBall()
	{
		return $this->db->get('balls')->result();
	}

	public function add($ball)
	{
		return $this->db->insert('balls', $ball);
	}

	public function getBallById($id)
	{
		return $this->db->query("SELECT * FROM balls where id=" . $id)->row();
	}
}
