<?php


class UserModel extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function checkEmail()
	{
		$userLogin['email'] = $this->input->post('email');
		$userLogin['password'] = $this->input->post('password');
		return $this->db->get_where('users', $userLogin)->row();
	}

}
