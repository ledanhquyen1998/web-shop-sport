<?php


class UserController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
	}

	public function login()
	{
		$this->load->view('user/login');
	}

	public function checkEmail()
	{
		$userLogin = $this->UserModel->checkEmail();
		if ($userLogin) {
			$this->session->set_userdata('user', $userLogin);
			redirect('home');
		} else {
			$this->session->set_flashdata('fail', 'Sai địa chỉ email hoặc mật khẩu');
			redirect('login');
		}
	}

	public function logOut()
	{
		$this->session->sess_destroy();
		redirect('home');

	}
}
