<?php


class ShoesController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ShoesModel');
	}

	public function uploadImage($image)
	{
		$config['upload_path'] = './shoes';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = true;
		$this->load->library('upload', $config);
		$this->upload->do_upload($image);
		return $this->upload->data();
	}

	public function formAdd()
	{
		return $this->load->view('shoes/formAdd');
	}

	public function create()
	{
		$image = $this->uploadImage('imageshose');
		$shoes = array(
			'titleshose' => $this->input->post('titleshose'),
			'priceshose' => $this->input->post('priceshose'),
			'imageshose' => implode(" ", $image)
		);
		if ($this->ShoesModel->add($shoes)) {
			redirect('home');
		} else {
			$this->session->set_flashdata('upload-clothes-fail', 'Thêm sản phẩm thất bại, vui lòng kiểm tra lại');
			$this->load->view('shoes/formAdd');
		}
	}

	public function shoesDetail($id)
	{
		$data['shoes'] = $this->ShoesModel->getShoesById($id);
		$this->load->view('shoes/shoesDetail', $data);
	}

	public function orderShoes($id)
	{
		$shoesWantToOrder = $this->ShoesModel->getShoesById($id);
		$titleShoes = $this->input->post('titleShoes');
		$numberShoes = $this->input->post('number');

		$arrayPriceShoes = [];

		if ($this->input->post('price_1')) {
			array_push($arrayPriceShoes, $this->input->post('price_1'));
		} else {
			$this->session->set_flashdata('check-radio-fail', 'Bạn phải chọn mẫu áo trước khi đặt hàng');
		}
		$from_email = "ledanhquyen1998@gmail.com";
		$to_email = $this->session->userdata('user')->email;

		$this->load->library('email');

		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		$config['smtp_user'] = 'ledanhquyen1998@gmail.com';
		$config['smtp_pass'] = 'Ledanhquyen98';
		$config['smtp_port'] = '465';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'text';

		$this->email->initialize($config);

		$this->email->from($from_email, 'Shop Sport Online');
		$this->email->to($to_email);
		$this->email->subject('Đặt Hàng Thành Công');
		$this->email->message('Bạn đã đặt hàng thành công ' .
			$numberShoes .
			' đôi ' .
			$shoesWantToOrder->titleshose .
			', giá 1 đôi là ' .
			$shoesWantToOrder->priceshose .
			', số tiền ' . $numberShoes .
			' đôi bạn phải thanh toán là ' .
			$numberShoes * (+$shoesWantToOrder->priceshose));

		if ($this->email->send()) {
			$this->session->set_flashdata("success",
				"Đặt Hàng Thành Công, 
				Vui Lòng Kiểm Tra Lại Mail Của Bạn !!!"
			);
		} else {
			$this->session->set_flashdata('fail',
				"Đặt Hàng Thất Bại,
				 Vui Lòng Kiểm Tra Lại"
			);
		}
		redirect('shoes-detail/' . $shoesWantToOrder->id);
	}
}
