<?php


class BallController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('BallModel');
	}

	public function uploadImage($image)
	{
		$config['upload_path'] = './ball';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = true;
		$this->load->library('upload', $config);
		$this->upload->do_upload($image);
		return $this->upload->data();
	}

	public function formAdd()
	{
		$this->load->view('ball/formAdd');
	}

	public function create()
	{
		$image = $this->uploadImage('imageball');
		$shoes = array(
			'titleball' => $this->input->post('titleball'),
			'priceball' => $this->input->post('priceball'),
			'imageball' => implode(" ", $image)
		);
		if ($this->BallModel->add($shoes)) {
			redirect('home');
		} else {
			$this->session->set_flashdata('upload-clothes-fail', 'Thêm sản phẩm thất bại, vui lòng kiểm tra lại');
			$this->load->view('ball/formAdd');
		}
	}

	public function ballDetail($id)
	{
		$data['ball'] = $this->BallModel->getBallById($id);
		$this->load->view('ball/ballDetail', $data);
	}

	public function orderBall($id)
	{
		$ballWantToOrder = $this->BallModel->getBallById($id);
		$numberBall = $this->input->post('number');

		$arrayPriceBall = [];

		if ($this->input->post('price_1')) {
			array_push($arrayPriceBall, $this->input->post('price_1'));
		} else {
			$this->session->set_flashdat('check-radio-fail', 'Bạn phải chọn mẫu áo trước khi đặt hàng');
		}
		$from_email = "ledanhquyen1998@gmail.com";
		$to_email = $this->session->userdata('user')->email;

		$this->load->library('email');

		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		$config['smtp_user'] = 'ledanhquyen1998@gmail.com';
		$config['smtp_pass'] = 'Ledanhquyen98';
		$config['smtp_port'] = '465';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'text';

		$this->email->initialize($config);

		$this->email->from($from_email, 'Shop Sport Online');
		$this->email->to($to_email);
		$this->email->subject('Đặt Hàng Thành Công');
		$this->email->message('Bạn đã đặt hàng thành công '
			. $numberBall
			. ' quả '
			. $ballWantToOrder->title
			. ' giá 1 quả là ' . $arrayPriceBall[0] . 'đ '
			. ' tổng số tiền ' . $numberBall . ' quả bạn phải thanh toán là '
			. $numberBall * (+$arrayPriceBall[0]) . ' đ');

		if ($this->email->send()) {
			$this->session->set_flashdata("success",
				"Đặt Hàng Thành Công, 
				Vui Lòng Kiểm Tra Lại Mail Của Bạn !!!"
			);
		} else {
			$this->session->set_flashdata('fail',
				"Đặt Hàng Thất Bại,
				 Vui Lòng Kiểm Tra Lại"
			);
		}

		redirect('ball-detail/'.$ballWantToOrder->id);
	}
}
