<?php


class ClothesController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ClothesModel');
		$this->load->model('TypeModel');
		$this->load->model('ShoesModel');
		$this->load->model('BallModel');
	}

	public function home()
	{
		$data['balls'] = $this->BallModel->getAllBall();
		$data['clothesCts'] = $this->ClothesModel->getClothesCt();
		$data['clothesClbs'] = $this->ClothesModel->getClothesClb();
		$data['listShoes'] = $this->ShoesModel->getAllShoes();
		return $this->load->view('viewMaster', $data);
	}

	public function formAdd()
	{
		$data['types'] = $this->TypeModel->getAll();
		return $this->load->view('clothes/formAdd', $data);
	}

	public function uploadImage($image)
	{
		$config['upload_path'] = './clothes';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = true;
		$this->load->library('upload', $config);
		$this->upload->do_upload($image);
		return $this->upload->data();
	}

	public function create()
	{
		$image = $this->uploadImage('image');
		$clothes = array(
			'title' => $this->input->post('title'),
			'price' => $this->input->post('price'),
			'numbers' => $this->input->post('numbers'),
			'type_id' => $this->input->post('type_id'),
			'image' => implode(" ", $image)
		);
		if ($this->ClothesModel->add($clothes)) {
			redirect('home');
		} else {
			$this->session->set_flashdata('upload-clothes-fail', 'Thêm sản phẩm thất bại, vui lòng kiểm tra lại');
			$this->load->view('clothes/formAdd');
		}
	}

	public function clothesDetail($id)
	{
		$data['clothesById'] = $this->ClothesModel->getClothesById($id);
		$this->load->view('clothes/clothesDetail', $data);
	}

	public function orderClothes($id)
	{
		$clothesWantToOrder = $this->ClothesModel->getClothesById($id);
		$titleClothes = $this->input->post('titleClothes');
		$numberClothes = $this->input->post('number');

		$arrayPriceClothes = [];

		if ($this->input->post('price_1')) {
			array_push($arrayPriceClothes, $this->input->post('price_1'));
		} else {
			$this->session->set_flashdat('check-radio-fail', 'Bạn phải chọn mẫu áo trước khi đặt hàng');
		}
		$from_email = "ledanhquyen1998@gmail.com";
		$to_email = $this->session->userdata('user')->email;

		$this->load->library('email');

		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		$config['smtp_user'] = 'ledanhquyen1998@gmail.com';
		$config['smtp_pass'] = 'Ledanhquyen98';
		$config['smtp_port'] = '465';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'text';

		$this->email->initialize($config);

		$this->email->from($from_email, 'Shop Sport Online');
		$this->email->to($to_email);
		$this->email->subject('Đặt Hàng Thành Công');
		$this->email->message('Bạn đã đặt hàng thành công '
			. $numberClothes
			. ' bộ '
			. $clothesWantToOrder->title
			. ' giá 1 bộ là ' . $arrayPriceClothes[0] . 'đ '
			. ' tổng số tiền ' . $numberClothes . ' bộ bạn phải thanh toán là '
			. $numberClothes * (+$arrayPriceClothes[0]) . ' đ');

		if ($this->email->send()) {
			$this->session->set_flashdata("success",
				"Đặt Hàng Thành Công, 
				Vui Lòng Kiểm Tra Lại Mail Của Bạn !!!"
			);
		} else {
			$this->session->set_flashdata('fail',
				"Đặt Hàng Thất Bại,
				 Vui Lòng Kiểm Tra Lại"
			);
		}

		redirect('clothes-detail/' . $clothesWantToOrder->id);
	}

	public function listClothesCt()
	{
		$data['clothesCts'] = $this->ClothesModel->getClothesCt();
		$this->load->view('clothes/listClothesCt', $data);
	}
}
